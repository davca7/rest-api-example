FROM openjdk:11
ADD target/dijkstra.jar dijkstra.jar
ENTRYPOINT ["java", "-jar", "dijkstra.jar"]
EXPOSE 8080
