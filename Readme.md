# Example Spring project

This example project contains is presenting example of REST API endpoint implementation.

## API Shortest path in graph

There are defined 2 endpoints allowing to find shortest path in a graph. See [OpenAPI specs](api.yaml) for details.

Full specification (in Czech) can be found [here](Zadání%20samostatné%20práce.pdf).

## Application use

Maven command to start application is as follows:

```shell
./mvnw spring-boot:run
```

Started API server **listens on port 8080**.

Example HTTP requests can be found in [example1.http](example1.http) and [example2.http](example2.http) files.

## Application packaging

```shell
./mvnw package

docker build -t dijkstra .

docker run --name graphs -p 8080:8080 dijkstra:latest
```
