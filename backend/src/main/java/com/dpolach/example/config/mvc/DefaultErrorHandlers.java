package com.dpolach.example.config.mvc;

import com.dpolach.example.config.mvc.response.ErrorResponse;
import com.dpolach.example.config.mvc.response.ResponseErrorDescription;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class DefaultErrorHandlers {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    ResponseEntity<ErrorResponse> handleValidationException(MethodArgumentNotValidException ex) {
        List<ResponseErrorDescription> errors = ex.getAllErrors()
                                                  .stream()
                                                  .map(error -> new ResponseErrorDescription(error.getCode(), error.getDefaultMessage()))
                                                  .collect(Collectors.toList());
        return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body(new ErrorResponse(errors));
    }

    private boolean isViolatedInController(ConstraintViolation<?> constraintViolation) {
        return constraintViolation.getRootBeanClass().isAnnotationPresent(Controller.class)
                || constraintViolation.getRootBeanClass().isAnnotationPresent(RestController.class);
    }

    private ResponseErrorDescription createErrorDescription(ConstraintViolation<?> violation) {
        // remove controller method name from propertyPath
        String parameterName = violation.getPropertyPath().toString().split("\\.", 2)[1];
        return new ResponseErrorDescription("InvalidRequestParameter", String.format("%s: %s", parameterName, violation.getMessage()));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse handleConstraintViolationException(ConstraintViolationException ex) {
        List<ResponseErrorDescription> errorDescriptions = ex.getConstraintViolations()
                                                             .stream()
                                                             .filter(this::isViolatedInController)
                                                             .map(this::createErrorDescription)
                                                             .collect(Collectors.toList());

        if (errorDescriptions.isEmpty()) {
            // do not respond with HTTP 400 for violations occurred at different place than controller
            throw ex;
        } else {
            return new ErrorResponse(errorDescriptions);
        }
    }

    @ExceptionHandler(ResponseStatusException.class)
    ResponseEntity<ErrorResponse> handleResponseStatusException(ResponseStatusException ex) {
        ErrorResponse body = ErrorResponse.createSingleErrorResponseBody(ex.getStatus().name(), ex.getReason());
        return ResponseEntity.status(ex.getStatus()).headers(ex.getResponseHeaders()).body(body);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse handleMissingServletRequestParameterException(MissingServletRequestParameterException ex) {
        return ErrorResponse.createSingleErrorResponseBody("MissingParameter", String.format("%s: parameter is required but it's value is missing", ex.getParameterName()));
    }
}
