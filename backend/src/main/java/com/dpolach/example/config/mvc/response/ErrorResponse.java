package com.dpolach.example.config.mvc.response;

import java.util.Collection;
import java.util.List;

public class ErrorResponse {
    public static ErrorResponse createSingleErrorResponseBody(String errorCode, String errorMessage) {
        ResponseErrorDescription description = new ResponseErrorDescription(errorCode, errorMessage);
        return new ErrorResponse(List.of(description));
    }

    private final Collection<ResponseErrorDescription> errors;

    public ErrorResponse(Collection<ResponseErrorDescription> errors) {
        this.errors = errors;
    }

    public Collection<ResponseErrorDescription> getErrors() {
        return errors;
    }
}
