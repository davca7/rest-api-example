package com.dpolach.example.config.mvc.response;

public class ResponseErrorDescription {
    private final String message;
    private final String code;

    public ResponseErrorDescription(String code, String message) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }
}
