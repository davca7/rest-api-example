package com.dpolach.example.graphs;

import com.dpolach.example.utils.matrix.Matrix;

import java.util.List;

public interface Graph {
    /**
     * @return vertices in the graph
     */
    List<Integer> getVertices();

    /**
     * @param vertexId
     * @return vertices with connection to given vertex
     * @throws IllegalArgumentException when vertex with given id doesn't exist in the graph
     */
    List<Integer> getVertexNeighbours(int vertexId);

    /**
     * @param startVertex
     * @param targetVertex
     * @return length of the connection between start and target vertex.
     * @throws IllegalArgumentException when either of vertexes doesn't exist in graph or connection between start and target vertex doesn't exist
     */
    int getDistanceBetween(int startVertex, int targetVertex);

    Matrix getAdjancencyWithDistancesMatrix();
}
