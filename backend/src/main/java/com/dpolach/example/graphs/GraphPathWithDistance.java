package com.dpolach.example.graphs;

import java.util.ArrayList;
import java.util.List;

public class GraphPathWithDistance {
    private final int distance;
    private final List<Integer> path;

    public GraphPathWithDistance(int distance, List<Integer> path) {
        this.distance = distance;
        this.path = path;
    }

    public int getDistance() {
        return distance;
    }

    public List<Integer> getPath() {
        return new ArrayList<>(path);
    }
}
