package com.dpolach.example.graphs;

import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.Optional;

@Validated
public interface GraphRepository {

    Graph save(@Valid Graph graphDefinition);

    Optional<Graph> find();

    void remove();
}
