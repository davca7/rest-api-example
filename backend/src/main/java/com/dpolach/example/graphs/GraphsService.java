package com.dpolach.example.graphs;

import java.util.Optional;

public interface GraphsService {

    /**
     * Defines graph structure. There is single graph definition which will be used by other methods from this service
     *
     * @param adjancencyMatrix
     * @return
     */
    Graph defineGraphFromAdjancencyMatrixData(int[][] adjancencyMatrix);

    Optional<Graph> getDefinedGraph();

    /**
     * @param targetNode
     * @return shortest path in graph from node 0 to targetNode. Empty optional is returned when targetNode doesn't exist in defined graph.
     * @throws UndefinedGraphException: thrown when graph definition is not defined (see {@link #defineGraphFromAdjancencyMatrixData(int[][])})
     */
    Optional<GraphPathWithDistance> getShortestPathToNode(int targetNode) throws UndefinedGraphException;
}
