package com.dpolach.example.graphs;

import com.dpolach.example.graphs.distanceCalculator.GraphDistanceDijkstraCalculator;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
class GraphsServiceImpl implements GraphsService {

    private final GraphRepository graphRepository;

    GraphsServiceImpl(GraphRepository graphRepository) {
        this.graphRepository = graphRepository;
    }

    @Override
    public Graph defineGraphFromAdjancencyMatrixData(int[][] adjancencyMatrix) {
        Graph graphDefinition = new ModifiedAdjancencyMatrixGraph(adjancencyMatrix);
        return graphRepository.save(graphDefinition);
    }

    @Override
    public Optional<Graph> getDefinedGraph() {
        return graphRepository.find();
    }

    @Override
    public Optional<GraphPathWithDistance> getShortestPathToNode(int targetNode) {
        Graph graphDefinition = graphRepository.find().orElseThrow(UndefinedGraphException::new);

        if (!graphDefinition.getVertices().contains(targetNode)) {
            return Optional.empty();
        }

        GraphDistanceDijkstraCalculator distanceCalculator = new GraphDistanceDijkstraCalculator(graphDefinition, 0);

        return Optional.of(distanceCalculator.getDistanceToVertex(targetNode));
    }
}
