package com.dpolach.example.graphs;

import com.dpolach.example.utils.matrix.Matrix;
import com.dpolach.example.validation.GraphAdjacencyWithLengthsMatrix;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Implementation for graph defined by modified adjancency matrix - that is matrix where non-zero positive value defines connection between two vertices and value defines length of such connection
 */
public class ModifiedAdjancencyMatrixGraph implements Graph {
    @GraphAdjacencyWithLengthsMatrix
    private final Matrix adjancencyWithEdgeLengthsMatrix;
    private final int verticesCount;

    public ModifiedAdjancencyMatrixGraph(int[][] modifiedAdjancencyMatrix) {
        this.adjancencyWithEdgeLengthsMatrix = new Matrix(modifiedAdjancencyMatrix);
        this.verticesCount = adjancencyWithEdgeLengthsMatrix.getRows();
    }

    private void validateVertexId(int vertexId) {
        if (vertexId < 0) {
            throw new IllegalArgumentException(String.format("VertexId can't be negative number - got " + vertexId));
        }
        if (vertexId >= verticesCount) {
            throw new IllegalArgumentException(String.format("Invalid vertex ID %d: graph has only %d vertexes", vertexId, adjancencyWithEdgeLengthsMatrix.getRows()));
        }
    }

    @Override
    public List<Integer> getVertices() {
        return IntStream.range(0, adjancencyWithEdgeLengthsMatrix.getRows()).boxed().collect(Collectors.toList());
    }

    @Override
    public List<Integer> getVertexNeighbours(int vertexId) {
        validateVertexId(vertexId);

        List<Integer> neighbourIds = new ArrayList<>();

        int[] modifiedEdgeLengths = adjancencyWithEdgeLengthsMatrix.getRow(vertexId);
        for (int neighbourId = 0; neighbourId < modifiedEdgeLengths.length; neighbourId++) {
            if (modifiedEdgeLengths[neighbourId] > 0) {
                neighbourIds.add(neighbourId);
            }
        }
        return neighbourIds;
    }

    @Override
    public int getDistanceBetween(int startVertex, int targetVertex) {
        validateVertexId(startVertex);
        validateVertexId(targetVertex);

        if (startVertex == targetVertex) {
            return 0;
        }

        int modifiedAdjancencyLength = adjancencyWithEdgeLengthsMatrix.get(startVertex, targetVertex);
        if (modifiedAdjancencyLength == 0) {
            throw new IllegalArgumentException("There is no edge from vertex " + startVertex + " to vertex " + targetVertex);
        }
        return modifiedAdjancencyLength;
    }

    @Override
    public Matrix getAdjancencyWithDistancesMatrix() {
        return new Matrix(adjancencyWithEdgeLengthsMatrix.getData());
    }
}
