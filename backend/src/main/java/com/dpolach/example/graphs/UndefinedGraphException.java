package com.dpolach.example.graphs;

public class UndefinedGraphException extends RuntimeException {
    public UndefinedGraphException() {
        super("Graph definition doesn't exist");
    }
}
