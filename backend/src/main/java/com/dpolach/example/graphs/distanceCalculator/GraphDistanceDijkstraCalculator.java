package com.dpolach.example.graphs.distanceCalculator;

import com.dpolach.example.graphs.Graph;
import com.dpolach.example.graphs.GraphPathWithDistance;

import java.util.*;
import java.util.stream.Collectors;

public class GraphDistanceDijkstraCalculator {

    private final GraphDistanceVertex[] vertices;

    public GraphDistanceDijkstraCalculator(Graph graphDefinition, int startingVertex) {
        if (!graphDefinition.getVertices().contains(startingVertex)) {
            throw new IllegalArgumentException(String.format("Given starting vertex %d is not defined in graph", startingVertex));
        }

        vertices = graphDefinition.getVertices()
                                  .stream()
                                  .map(GraphDistanceVertex::new)
                                  .toArray(GraphDistanceVertex[]::new);
        vertices[startingVertex] = new GraphDistanceVertex(startingVertex, 0);

        Set<GraphDistanceVertex> completedVertices = new HashSet<>();

        Queue<GraphDistanceVertex> processingQueue = new LinkedList<>();
        processingQueue.add(vertices[startingVertex]);

        while (!processingQueue.isEmpty()) {

            processingQueue = sortByDistanceFromStart(processingQueue);
            GraphDistanceVertex processingVertex = processingQueue.poll();
            completedVertices.add(processingVertex);

            for (GraphDistanceVertex neighbourVertex : getNeighbourVerticesFor(processingVertex, graphDefinition)) {
                if (!completedVertices.contains(neighbourVertex)) {
                    int edgeDistance = graphDefinition.getDistanceBetween(processingVertex.getId(), neighbourVertex.getId());

                    int distanceToNeighbourThroughProcessedVertex = processingVertex.getDistanceFromStart() + edgeDistance;
                    if (neighbourVertex.getDistanceFromStart() > distanceToNeighbourThroughProcessedVertex) {
                        neighbourVertex.setDistance(distanceToNeighbourThroughProcessedVertex, processingVertex);
                        processingQueue.add(neighbourVertex);
                    }
                }
            }
        }
    }

    private Queue<GraphDistanceVertex> sortByDistanceFromStart(Collection<GraphDistanceVertex> processingQueue) {
        return processingQueue.stream()
                              .sorted(Comparator.comparing(GraphDistanceVertex::getDistanceFromStart))
                              .collect(Collectors.toCollection(LinkedList::new));
    }

    private List<GraphDistanceVertex> getNeighbourVerticesFor(GraphDistanceVertex vertex, Graph graph) {
        return graph.getVertexNeighbours(vertex.getId())
                    .stream()
                    .map(vertexId -> vertices[vertexId])
                    .collect(Collectors.toList());
    }

    public GraphPathWithDistance getDistanceToVertex(int targetVertex) {
        if (targetVertex >= vertices.length) {
            throw new IllegalArgumentException(String.format("Target vertex %d is not defined in graph", targetVertex));
        }

        return new GraphPathWithDistance(vertices[targetVertex].getDistanceFromStart(), vertices[targetVertex].getPathFromStart());
    }
}
