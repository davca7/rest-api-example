package com.dpolach.example.graphs.distanceCalculator;

import com.dpolach.example.graphs.GraphPathWithDistance;
import org.springframework.lang.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

class GraphDistanceVertex {
    private final int id;
    private int distanceFromStart;
    private GraphDistanceVertex previousNode;

    public GraphDistanceVertex(int id) {
        this(id, Integer.MAX_VALUE);
    }

    public GraphDistanceVertex(int id, int initialDistance) {
        this.id = id;
        distanceFromStart = initialDistance;
        previousNode = null;
    }

    public int getId() {
        return id;
    }

    public int getDistanceFromStart() {
        return distanceFromStart;
    }

    public Optional<GraphDistanceVertex> getPreviousVertex() {
        return Optional.ofNullable(previousNode);
    }

    public synchronized void setDistance(int newDistance, @NonNull GraphDistanceVertex predecessor) {
        if (this.id == predecessor.id) {
            throw new IllegalArgumentException("Predecessor must not be same vertex - current vertex is " + id + " and predecessor is " + predecessor.id);
        } else if (predecessor.getPathFromStart().contains(this.id)) {
            throw new IllegalArgumentException("Circles are not allowed in GraphDistanceVertex - current vertex " + id + " is already in path " + predecessor.getPathFromStart() + " of given predecessor " + predecessor.id);
        }

        distanceFromStart = newDistance;
        this.previousNode = predecessor;
    }

    public List<Integer> getPathFromStart() {
        if (previousNode == null) {
            return new ArrayList<>();
        } else {
            List<Integer> previousNodePath = previousNode.getPathFromStart();
            previousNodePath.add(previousNode.getId());
            return previousNodePath;
        }
    }

    public GraphPathWithDistance getPathWithDistanceFromStart() {
        if (previousNode == null) {
            return new GraphPathWithDistance(distanceFromStart, List.of());
        } else {
            return new GraphPathWithDistance(distanceFromStart, getPathFromStart());
        }
    }
}
