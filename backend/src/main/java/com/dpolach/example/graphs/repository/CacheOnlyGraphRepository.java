package com.dpolach.example.graphs.repository;

import com.dpolach.example.graphs.Graph;
import com.dpolach.example.graphs.GraphRepository;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import javax.validation.Valid;
import java.util.Optional;

@CacheConfig(cacheNames = CacheOnlyGraphRepository.GRAPHS_CACHE_NAME)
@Repository
class CacheOnlyGraphRepository implements GraphRepository {

    static final String GRAPHS_CACHE_NAME = "graph-definitions";

    private static final String GRAPH_DEFINITION_CACHE_KEY = "'savedGraph'";

    @CachePut(key = GRAPH_DEFINITION_CACHE_KEY)
    @Override
    public Graph save(@Valid Graph graphDefinition) {
        return graphDefinition;
    }

    @Cacheable(key = GRAPH_DEFINITION_CACHE_KEY)
    @Override
    public Optional<Graph> find() {
        return Optional.empty();
    }

    @CacheEvict(key = GRAPH_DEFINITION_CACHE_KEY)
    @Override
    public void remove() {

    }
}
