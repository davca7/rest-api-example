package com.dpolach.example.graphs.restapi;

import com.dpolach.example.config.mvc.response.ErrorResponse;
import com.dpolach.example.graphs.Graph;
import com.dpolach.example.graphs.GraphsService;
import com.dpolach.example.graphs.UndefinedGraphException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/graph")
class GraphController {

    private final GraphsService graphService;

    public GraphController(GraphsService graphService) {
        this.graphService = graphService;
    }

    @GetMapping
    public GraphDefinitionResponse getGraph() {
        Graph graph = graphService.getDefinedGraph().orElseThrow(UndefinedGraphException::new);

        return new GraphDefinitionResponse(graph.getAdjancencyWithDistancesMatrix().getData());
    }

    @ExceptionHandler(UndefinedGraphException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handleUndefinedGraphException(UndefinedGraphException ex) {
        return ErrorResponse.createSingleErrorResponseBody("GraphNotDefined", ex.getMessage());
    }
}
