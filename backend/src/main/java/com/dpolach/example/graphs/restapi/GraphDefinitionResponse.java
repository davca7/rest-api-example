package com.dpolach.example.graphs.restapi;

class GraphDefinitionResponse {
    private int[][] matrix;

    public GraphDefinitionResponse(int[][] matrix) {
        this.matrix = matrix;
    }

    public int[][] getMatrix() {
        return matrix;
    }
}
