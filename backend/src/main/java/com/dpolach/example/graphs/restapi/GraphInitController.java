package com.dpolach.example.graphs.restapi;

import com.dpolach.example.graphs.Graph;
import com.dpolach.example.graphs.GraphsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/graph/init")
class GraphInitController {

    private final GraphsService graphService;

    public GraphInitController(GraphsService graphService) {
        this.graphService = graphService;
    }

    @PostMapping
    public PostGraphInitResponse postGraphInit(@RequestBody @Valid PostGraphInitRequest body) {
        Graph definedGraph = graphService.defineGraphFromAdjancencyMatrixData(body.getGraph());
        return new PostGraphInitResponse("Graph definition updated", definedGraph.getVertices());
    }

}
