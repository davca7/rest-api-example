package com.dpolach.example.graphs.restapi;

import com.dpolach.example.config.mvc.response.ErrorResponse;
import com.dpolach.example.graphs.GraphPathWithDistance;
import com.dpolach.example.graphs.GraphsService;
import com.dpolach.example.graphs.UndefinedGraphException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("/graph/path")
@Validated
class GraphPathController {

    private final GraphsService graphService;

    public GraphPathController(GraphsService graphService) {
        this.graphService = graphService;
    }

    @GetMapping
    public ResponseEntity<GraphPathWithDistance> getShortestPathToNode(@Valid @RequestParam("targetNode") @Min(0) Integer targetNode) {
        return graphService.getShortestPathToNode(targetNode)
                           .map(ResponseEntity::ok)
                           .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "targetNode: graph node with id " + targetNode + " doesn't exist in defined graph"));
    }

    @ExceptionHandler(UndefinedGraphException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse handleUndefinedGraphException(UndefinedGraphException ex) {
        return ErrorResponse.createSingleErrorResponseBody("GraphNotDefined", ex.getMessage());
    }
}
