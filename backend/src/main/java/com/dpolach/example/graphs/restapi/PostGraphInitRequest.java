package com.dpolach.example.graphs.restapi;

import com.dpolach.example.validation.GraphAdjacencyWithLengthsMatrix;

import javax.validation.constraints.NotNull;

class PostGraphInitRequest {
    @NotNull
    @GraphAdjacencyWithLengthsMatrix
    private int[][] graph;

    public int[][] getGraph() {
        return graph;
    }

    public void setGraph(int[][] graph) {
        this.graph = graph;
    }

}
