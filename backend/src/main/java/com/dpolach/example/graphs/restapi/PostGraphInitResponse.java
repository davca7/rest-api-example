package com.dpolach.example.graphs.restapi;

import java.util.List;

class PostGraphInitResponse {
    private final String message;
    private final List<Integer> savedGraphVertices;

    PostGraphInitResponse(String message, List<Integer> graphVertices) {
        this.message = message;
        this.savedGraphVertices = graphVertices;
    }

    public String getMessage() {
        return message;
    }

    public List<Integer> getSavedGraphVertices() {
        return savedGraphVertices;
    }
}
