package com.dpolach.example.utils;

import java.util.function.Function;

public class ArraysUtil {

    private ArraysUtil() {
    }

    public static <T> void fill(T[] items, Function<Integer, T> sourceValueForIndex) {
        for (int arrayIndex = 0; arrayIndex < items.length; arrayIndex++) {
            items[arrayIndex] = sourceValueForIndex.apply(arrayIndex);
        }
    }

}
