package com.dpolach.example.utils.matrix;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Matrix {

    private final int[][] data;
    private final int rows;
    private final int cols;

    public Matrix(int[][] data) {
        this.rows = data.length;
        if (this.rows < 1) {
            throw new IllegalArgumentException(String.format("Minimal number of rows for Matrix is 1, got %d rows", rows));
        }

        this.cols = data[0].length;
        for (int row = 0; row < rows; row++) {
            int[] rowItems = data[row];
            if (rowItems.length != cols) {
                throw new IllegalArgumentException(String.format("All rows in matrix must have same length, 1st row has %d columns, row %d has %d columns!", cols, row, rowItems.length));
            }
        }

        this.data = data;
    }

    public int[][] getData() {
        return data;
    }

    public int get(int col, int row) {
        return data[row][col];
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    public int[] getRow(int rowIndex) {
        return data[rowIndex];
    }

    public int[] getCol(int colIndex) {
        int[] result = new int[getRows()];
        for (int rowIndex = 0; rowIndex < rows; rowIndex++) {
            result[rowIndex] = get(colIndex, rowIndex);
        }
        return result;
    }

    public void set(int col, int row, int value) {
        data[row][col] = value;
    }

    @Override
    public String toString() {
        return StreamSupport.stream(Arrays.spliterator(data), false)
                            .map(Arrays::toString)
                            .collect(Collectors.joining("\n", "===========\n", ""));
    }
}
