package com.dpolach.example.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@SquareMatrix
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {DiagonalHollowMatrixConstraintForIntArray.class, DiagonalHollowMatrixConstraint.class})
public @interface DiagonalHollowMatrix {

    String message() default "there are non-zero values on main diagonal";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
