package com.dpolach.example.validation;

import com.dpolach.example.utils.matrix.Matrix;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class DiagonalHollowMatrixConstraint implements ConstraintValidator<DiagonalHollowMatrix, Matrix> {
    private final DiagonalHollowMatrixConstraintForIntArray intConstraint = new DiagonalHollowMatrixConstraintForIntArray();

    @Override
    public boolean isValid(Matrix adjancencyData, ConstraintValidatorContext constraintValidatorContext) {
        return intConstraint.isValid(adjancencyData.getData(), constraintValidatorContext);
    }
}
