package com.dpolach.example.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class DiagonalHollowMatrixConstraintForIntArray implements ConstraintValidator<DiagonalHollowMatrix, int[][]> {
    @Override
    public void initialize(DiagonalHollowMatrix constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    private String formatMatrixErrorMessageWithCoordinates(int x, int y, String errorMessage) {
        return String.format("[" + x + ", " + y + "]: " + errorMessage);
    }

    @Override
    public boolean isValid(int[][] matrixData, ConstraintValidatorContext constraintValidatorContext) {
        if (matrixData == null) {
            return true;
        }

        constraintValidatorContext.disableDefaultConstraintViolation();

        boolean isValid = true;
        for (int i = 0; i < matrixData.length; i++) {
            if (matrixData[i][i] != 0) {
                isValid = false;
                String violationMessage = formatMatrixErrorMessageWithCoordinates(i, i, "only zero values expected on main diagonal, got " + matrixData[i][i]);
                constraintValidatorContext.buildConstraintViolationWithTemplate(violationMessage)
                                          .addConstraintViolation();
            }
        }
        return isValid;
    }
}
