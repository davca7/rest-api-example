package com.dpolach.example.validation;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Validates "adjancency matrix with length values" data. That is "mix" of graph adjacency matrix and edge-lengths matrix where non-zero value represents both existence of edge between two nodes and length of that edge. Zero value then represents non-existence of edge between two nodes.
 */
@DiagonalHollowMatrix
@Constraint(validatedBy = {GraphEdgeLengthsMatrixConstraintForIntArray.class, GraphEdgeLengthsMatrixConstraint.class})
@Documented
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface GraphAdjacencyWithLengthsMatrix {
    String message() default "Invalid adjancency matrix data";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * Defines is validated value should represent directed graph. Difference is that directed graph means that matrix is symmetrical while undirected graph doesn't have such requirement.
     *
     * @return
     */
    boolean directedGraph() default false;
}
