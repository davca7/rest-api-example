package com.dpolach.example.validation;

import com.dpolach.example.utils.matrix.Matrix;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class GraphEdgeLengthsMatrixConstraint implements ConstraintValidator<GraphAdjacencyWithLengthsMatrix, Matrix> {

    private final GraphEdgeLengthsMatrixConstraintForIntArray intConstraint = new GraphEdgeLengthsMatrixConstraintForIntArray();

    @Override
    public boolean isValid(Matrix ints, ConstraintValidatorContext constraintValidatorContext) {
        return intConstraint.isValid(ints.getData(), constraintValidatorContext);
    }
}
