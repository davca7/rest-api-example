package com.dpolach.example.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class GraphEdgeLengthsMatrixConstraintForIntArray implements ConstraintValidator<GraphAdjacencyWithLengthsMatrix, int[][]> {

    private final SymmetricalMatrixConstraintForIntArray symmetricalMatrixConstraint = new SymmetricalMatrixConstraintForIntArray();

    private boolean isOrientedGraph = false;

    @Override
    public void initialize(GraphAdjacencyWithLengthsMatrix constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
        this.isOrientedGraph = constraintAnnotation.directedGraph();
    }

    private String createNegativeValueViolationMessage(int[][] matrixData, int violatedCoordX, int violatedCoordY) {
        return String.format(String.format("expected positive or zero values only, got %d at coordinates [%d,%d]", matrixData[violatedCoordX][violatedCoordY], violatedCoordX, violatedCoordY));
    }

    private boolean hasOnlyPositiveOrZeroValue(int[][] matrixData, ConstraintValidatorContext constraintValidatorContext) {

        boolean isValid = true;

        for (int x = 0; x < matrixData.length; x++) {
            for (int y = 0; y < matrixData[x].length; y++) {
                if (matrixData[x][y] < 0) {
                    isValid = false;
                    String errorMessage = createNegativeValueViolationMessage(matrixData, x, y);
                    constraintValidatorContext.buildConstraintViolationWithTemplate(errorMessage)
                                              .addConstraintViolation();
                }
            }
        }
        return isValid;
    }

    @Override
    public boolean isValid(int[][] ints, ConstraintValidatorContext constraintValidatorContext) {
        if (ints == null) {
            return true;
        }

        constraintValidatorContext.disableDefaultConstraintViolation();

        boolean isValid = true;

        if (!isOrientedGraph) {
            isValid = symmetricalMatrixConstraint.isValid(ints, constraintValidatorContext);
        }

        isValid &= hasOnlyPositiveOrZeroValue(ints, constraintValidatorContext);

        return isValid;
    }
}
