package com.dpolach.example.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {SquareMatrixConstraintForIntArray.class, SquareMatrixConstraint.class})
public @interface SquareMatrix {

    String message() default "not square matrix";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
