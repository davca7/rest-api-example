package com.dpolach.example.validation;

import com.dpolach.example.utils.matrix.Matrix;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class SquareMatrixConstraint implements ConstraintValidator<SquareMatrix, Matrix> {

    private final SquareMatrixConstraintForIntArray intConstraint = new SquareMatrixConstraintForIntArray();

    @Override
    public boolean isValid(Matrix adjancencyData, ConstraintValidatorContext constraintValidatorContext) {
        return intConstraint.isValid(adjancencyData.getData(), constraintValidatorContext);
    }
}
