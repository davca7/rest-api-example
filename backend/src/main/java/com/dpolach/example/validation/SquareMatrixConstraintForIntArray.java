package com.dpolach.example.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class SquareMatrixConstraintForIntArray implements ConstraintValidator<SquareMatrix, int[][]> {
    @Override
    public void initialize(SquareMatrix constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(int[][] matrixData, ConstraintValidatorContext constraintValidatorContext) {
        if (matrixData == null) {
            return true;
        }

        for (int[] matrixDatum : matrixData) {
            if (matrixDatum.length != matrixData.length) {
                return false;
            }
        }
        return true;
    }
}
