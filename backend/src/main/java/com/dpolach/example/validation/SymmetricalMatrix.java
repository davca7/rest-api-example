package com.dpolach.example.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@SquareMatrix
@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {SymmetricalMatrixConstraintForIntArray.class, SymmetricalMatrixConstraint.class})
public @interface SymmetricalMatrix {

    String message() default "matrix is not symmetrical";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
