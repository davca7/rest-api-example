package com.dpolach.example.validation;

import com.dpolach.example.utils.matrix.Matrix;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class SymmetricalMatrixConstraint implements ConstraintValidator<SymmetricalMatrix, Matrix> {

    private final SymmetricalMatrixConstraintForIntArray intConstraint = new SymmetricalMatrixConstraintForIntArray();

    @Override
    public boolean isValid(Matrix adjancencyData, ConstraintValidatorContext constraintValidatorContext) {
        return intConstraint.isValid(adjancencyData.getData(), constraintValidatorContext);
    }
}
