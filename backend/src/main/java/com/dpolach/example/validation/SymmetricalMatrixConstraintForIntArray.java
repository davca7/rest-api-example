package com.dpolach.example.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class SymmetricalMatrixConstraintForIntArray implements ConstraintValidator<SymmetricalMatrix, int[][]> {

    @Override
    public void initialize(SymmetricalMatrix constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    private String createSymmetricMatrixViolationMessage(int[][] matrixData, int x, int y) {
        return String.format(String.format("asymmetrical value at coordinates [%d,%d] = %d and [%d,%d] = %d", x, y, matrixData[y][x], y, x, matrixData[x][y]));
    }

    @Override
    public boolean isValid(int[][] matrixData, ConstraintValidatorContext constraintValidatorContext) {
        if (matrixData == null) {
            return true;
        }

        constraintValidatorContext.disableDefaultConstraintViolation();

        boolean isValid = true;
        for (int x = 0; x < matrixData.length; x++) {
            for (int y = 0; y <= x; y++) {
                if (matrixData[x][y] != matrixData[y][x]) {
                    isValid = false;
                    String message = createSymmetricMatrixViolationMessage(matrixData, x, y);
                    constraintValidatorContext.buildConstraintViolationWithTemplate(message).addConstraintViolation();
                }
            }
        }
        return isValid;
    }
}
