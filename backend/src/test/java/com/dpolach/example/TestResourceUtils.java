package com.dpolach.example;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class TestResourceUtils {

    private TestResourceUtils() {
    }

    public static String readTestResourceFileContents(String filePath) throws IOException {
        try (InputStream resourceFileContents = TestResourceUtils.class.getResourceAsStream(filePath)) {
            return new String(resourceFileContents.readAllBytes(), StandardCharsets.UTF_8);
        }
    }

}
