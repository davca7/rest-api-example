package com.dpolach.example.graphs;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@DisplayName("GraphsServiceImpl tests")
@ExtendWith(MockitoExtension.class)
class GraphDistanceDijkstraCalculatorTest {

    @Mock
    private GraphRepository graphRepository;

    @InjectMocks
    private GraphsServiceImpl testedService;

    @DisplayName("defineGraphFromAdjancencyMatrixData tests")
    @Nested
    class DefineGraphFromAdjancencyMatrixDataTests {

    }

    @DisplayName("getShortestPathToNode(targetNode) tests")
    @Nested
    class GetShortestPathToNodeTests {

        void assertExpectedDistanceWithPathBetween(int expectedDistance, List<Integer> expectedPath, GraphsService testedService, int targetVertexId) {
            GraphPathWithDistance actualValue = testedService.getShortestPathToNode(targetVertexId).orElse(null);

            assertAll(
                    () -> assertEquals(expectedDistance, actualValue.getDistance(), "distance for target vertexId " + targetVertexId),
                    () -> assertIterableEquals(expectedPath, actualValue.getPath(), "path for target vertexId " + targetVertexId)
            );

        }

        @DisplayName("it should throw UndefinedGraphException exception when graph is NOT defined")
        @Test
        void itShouldCalculateCorrectDistanceAndPathForUndefinedGraph() {
            assertThrows(UndefinedGraphException.class, () -> testedService.getShortestPathToNode(2));
        }

        @DisplayName("it should return empty optional when given targetNode is not defined in defined graph")
        @Test
        void itShouldReturnEmptyOptionalWhenNodeIsUnnknownInDefinedGraph() {
            when(graphRepository.find()).thenReturn(Optional.of(TestGraphsMother.createTestGraphFromEdgeLengths(2, 0, 1, 1, 0)));

            assertEquals(Optional.empty(), testedService.getShortestPathToNode(3));
        }

        @DisplayName("it should calculate correct distance and path values when graph is defined")
        @Test
        void itShouldCalculateCorrectDistanceAndPath() {
            Graph exampleGraph = TestGraphsMother.create2ndExampleGraphFromSpecification();
            when(graphRepository.find()).thenReturn(Optional.of(exampleGraph));

            assertAll(
                    () -> assertExpectedDistanceWithPathBetween(0, List.of(), testedService, 0),
                    () -> assertExpectedDistanceWithPathBetween(3, List.of(0, 3), testedService, 1),
                    () -> assertExpectedDistanceWithPathBetween(7, List.of(0, 3, 4), testedService, 2),
                    () -> assertExpectedDistanceWithPathBetween(1, List.of(0), testedService, 3),
                    () -> assertExpectedDistanceWithPathBetween(2, List.of(0, 3), testedService, 4)
            );

        }
    }
}
