package com.dpolach.example.graphs;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("ModifiedAdjancencyMatrixGraph tests")
class ModifiedAdjancencyMatrixGraphTest {

    @DisplayName("getVertices() tests")
    @Nested
    class GetVerticesCountTests {
        @DisplayName("it should return correct list of vertex ids")
        @Test
        void itShouldReturnCorrectVertexIds() {
            ModifiedAdjancencyMatrixGraph testedGraph = TestGraphsMother.createTestGraphFromEdgeLengths(3, 0, 0, 0, 0, 0, 0, 0, 0, 0);

            assertIterableEquals(List.of(0, 1, 2), testedGraph.getVertices());
        }
    }

    @DisplayName("getVertexNeighbours(vertexId) tests")
    @Nested
    class GetVertexNeighboursTests {
        @DisplayName("it should return correct neighbours")
        @Test
        void itShouldReturnCorrectNeighbours() {
            ModifiedAdjancencyMatrixGraph testedGraph = TestGraphsMother.create2ndExampleGraphFromSpecification();

            assertAll(
                    () -> assertIterableEquals(List.of(1, 3), testedGraph.getVertexNeighbours(0), "neighbours of vertex 0"),
                    () -> assertIterableEquals(List.of(0, 2, 3, 4), testedGraph.getVertexNeighbours(1), "neighbours of vertex 1"),
                    () -> assertIterableEquals(List.of(1, 4), testedGraph.getVertexNeighbours(2), "neighbours of vertex 2"),
                    () -> assertIterableEquals(List.of(0, 1, 4), testedGraph.getVertexNeighbours(3), "neighbours of vertex 3"),
                    () -> assertIterableEquals(List.of(1, 2, 3), testedGraph.getVertexNeighbours(4), "neighbours of vertex 4")
            );
        }

        @DisplayName("it should throw IllegalArgumentException for invalid vertexId")
        @ParameterizedTest
        @ValueSource(ints = {-1, 4})
        void itShouldThrowIllegalArgumentExceptionForinvalidVerteexId(int invalidVertexId) {
            ModifiedAdjancencyMatrixGraph testedGraph = TestGraphsMother.createTestGraphFromEdgeLengths(3, 0, 0, 0, 0, 0, 0, 0, 0, 0);

            assertThrows(IllegalArgumentException.class, () -> testedGraph.getVertexNeighbours(invalidVertexId));
        }
    }

    @DisplayName("getDistanceBetween(startVertex, endVertex) Tests")
    @Nested
    class GetEdgeLengthTeests {
        @DisplayName("it should throw IllegalArgumentException for invalid start vertexId")
        @ParameterizedTest
        @ValueSource(ints = {-1, 4})
        void itShouldThrowIllegalArgumentExceptionForinvalidStartVerteexId(int invalidVertexId) {
            ModifiedAdjancencyMatrixGraph testedGraph = TestGraphsMother.createTestGraphFromEdgeLengths(3, 0, 0, 0, 0, 0, 0, 0, 0, 0);

            assertThrows(IllegalArgumentException.class, () -> testedGraph.getDistanceBetween(invalidVertexId, 0));
        }

        @DisplayName("it should throw IllegalArgumentException for invalid target vertexId")
        @ParameterizedTest
        @ValueSource(ints = {-1, 3})
        void itShouldThrowIllegalArgumentExceptionForinvalidTargetVerteexId(int invalidVertexId) {
            ModifiedAdjancencyMatrixGraph testedGraph = TestGraphsMother.createTestGraphFromEdgeLengths(2, 0, 0, 0, 0);

            assertThrows(IllegalArgumentException.class, () -> testedGraph.getDistanceBetween(0, invalidVertexId));
        }

        private void assertEdgeLength(int expectedValue, int vertexFrom, int vertexTo, ModifiedAdjancencyMatrixGraph graph) {
            assertEquals(expectedValue, graph.getDistanceBetween(vertexFrom, vertexTo), String.format("edge from %d to %d", vertexFrom, vertexTo));
        }

        private void assertNoEdgeBetween(int vertexFrom, int vertexTo, ModifiedAdjancencyMatrixGraph graph) {
            assertThrows(IllegalArgumentException.class, () -> graph.getDistanceBetween(vertexFrom, vertexTo));
        }

        @DisplayName("it should return correct edge lengths")
        @Test
        void itShouldReturnCorrectEdgeLengths() {
            ModifiedAdjancencyMatrixGraph exampleGraph = TestGraphsMother.create2ndExampleGraphFromSpecification();

            assertAll(
                    () -> assertEdgeLength(1, 3, 0, exampleGraph),
                    () -> assertEdgeLength(2, 3, 1, exampleGraph),
                    () -> assertNoEdgeBetween(3, 2, exampleGraph),
                    () -> assertEdgeLength(0, 3, 3, exampleGraph),
                    () -> assertEdgeLength(1, 3, 4, exampleGraph),

                    () -> assertNoEdgeBetween(4, 0, exampleGraph),
                    () -> assertEdgeLength(2, 4, 1, exampleGraph),
                    () -> assertEdgeLength(5, 4, 2, exampleGraph),
                    () -> assertEdgeLength(1, 4, 3, exampleGraph),
                    () -> assertEdgeLength(0, 4, 4, exampleGraph)

            );
        }
    }
}
