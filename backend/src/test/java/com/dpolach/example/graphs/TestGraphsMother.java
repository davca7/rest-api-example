package com.dpolach.example.graphs;

import com.dpolach.example.graphs.utils.matrix.MatrixMother;

public class TestGraphsMother {

    private TestGraphsMother() {
    }


    /**
     * @return graph with same structure as defined in specification as 2nd example
     */
    static ModifiedAdjancencyMatrixGraph create2ndExampleGraphFromSpecification() {
        int[][] graphData = new int[][]{
                new int[]{0, 6, 0, 1, 0},
                new int[]{6, 0, 5, 2, 2},
                new int[]{0, 5, 0, 0, 5},
                new int[]{1, 2, 0, 0, 1},
                new int[]{0, 2, 5, 1, 0}
        };

        return new ModifiedAdjancencyMatrixGraph(graphData);
    }

    static ModifiedAdjancencyMatrixGraph createTestGraphFromEdgeLengths(int verticesCount, int... edgeLengths) {
        return new ModifiedAdjancencyMatrixGraph(MatrixMother.createMatrixData(verticesCount, edgeLengths));
    }
}
