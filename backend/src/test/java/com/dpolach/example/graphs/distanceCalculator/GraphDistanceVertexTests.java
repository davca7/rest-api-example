package com.dpolach.example.graphs.distanceCalculator;

import com.dpolach.example.graphs.GraphPathWithDistance;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("GraphDistanceVertex tests")
class GraphDistanceVertexTests {

    @DisplayName("getId() tests")
    @Nested
    class GetIdTests {
        @DisplayName("it should return vertex's id")
        @Test
        void itShouldReturnExpectedValue() {
            final int expectedVertexId = 3;
            GraphDistanceVertex testedInstance = new GraphDistanceVertex(expectedVertexId);

            assertEquals(expectedVertexId, testedInstance.getId());
        }
    }

    @DisplayName("getDistanceFromStart() tests")
    @Nested
    class GetDistanceFromStartTests {
        @DisplayName("it should return maximal integer value for new instance")
        @Test
        void itShouldReturnMaximalIntegerForNewInstance() {
            GraphDistanceVertex testedInstance = new GraphDistanceVertex(1);

            assertEquals(Integer.MAX_VALUE, testedInstance.getDistanceFromStart());
        }

        @DisplayName("it should return distance set by method `setDistance(distance, previousNode)` ")
        @Test
        void itShouldReturnDistanceSetBySetDistanceMethod() {
            final int settedDistance = 23;
            GraphDistanceVertex testedInstance = new GraphDistanceVertex(1);

            testedInstance.setDistance(settedDistance, new GraphDistanceVertex(2));

            assertEquals(23, testedInstance.getDistanceFromStart());
        }

    }

    @DisplayName("getPreviousVertex() tests")
    @Nested
    class GetPreviousVertexTests {
        @DisplayName("it should return empty optional value for new instance")
        @Test
        void itShouldReturnEmptyOptionalForNewInstance() {
            GraphDistanceVertex testedInstance = new GraphDistanceVertex(1);

            assertEquals(Optional.empty(), testedInstance.getPreviousVertex());
        }

        @DisplayName("it should return previous node set by method `setDistance(distance, previousNode)` ")
        @Test
        void itShouldReturnPreviousNodeSetBySetDistanceMethod() {
            final GraphDistanceVertex expectedPreviousNode = new GraphDistanceVertex(2);
            GraphDistanceVertex testedInstance = new GraphDistanceVertex(1);

            testedInstance.setDistance(42, expectedPreviousNode);

            assertEquals(Optional.of(expectedPreviousNode), testedInstance.getPreviousVertex());
        }

    }

    @DisplayName("setDistance tests")
    @Nested
    class SetDistanceTests {
        @DisplayName("it should throw IllegalArgumentException when previous node closes circle in the graph (it would prevent proper backtracking to the node from which was distance measured from)")
        @Test
        void itShouldThrowIllegalArgumentExceptionWhenAddedPreviousNodeClosesCircleInTheGrap() {

            GraphDistanceVertex startVertex0 = new GraphDistanceVertex(0);
            GraphDistanceVertex pathVertex1 = new GraphDistanceVertex(1);
            pathVertex1.setDistance(3, startVertex0);

            GraphDistanceVertex pathVertex2 = new GraphDistanceVertex(2);
            pathVertex2.setDistance(2, pathVertex1);

            assertThrows(IllegalArgumentException.class, () -> startVertex0.setDistance(10, pathVertex2));
        }
    }

    @DisplayName("getPathFromStart tests")
    @Nested
    class GetPathFromStartTests {
        @DisplayName("it should return empty list for new instance")
        @Test
        void itShouldReturnEmptyListForNewInstance() {
            GraphDistanceVertex testedInstance = new GraphDistanceVertex(1);

            assertIterableEquals(List.of(), testedInstance.getPathFromStart());
        }

        @DisplayName("it should return list of vertexIds created from chain of previous vertexes` ")
        @Test
        void itShouldReturnPreviousNodeSetBySetDistanceMethod() {
            GraphDistanceVertex startVertex = new GraphDistanceVertex(2);

            GraphDistanceVertex pathItem1 = new GraphDistanceVertex(4);
            pathItem1.setDistance(2, startVertex);

            GraphDistanceVertex testedVertex = new GraphDistanceVertex(1);
            testedVertex.setDistance(5, pathItem1);

            assertIterableEquals(List.of(2, 4), testedVertex.getPathFromStart());
        }
    }

    @DisplayName("getPathWithDistanceFromStart() tests")
    @Nested
    class GetPathWithDistanceFromStartTests {

        private void assertGraphPathDistance(int expectedDistance, List<Integer> expectedPath, GraphPathWithDistance actualValue) {
            assertAll(
                    () -> assertEquals(expectedDistance, actualValue.getDistance(), "distance"),
                    () -> assertIterableEquals(expectedPath, actualValue.getPath(), "path")
            );

        }

        @DisplayName("it should return empty path with maximal integer as distance value")
        @Test
        void itShouldReturnEmptyPathForSingleVertex() {
            GraphDistanceVertex vertex = new GraphDistanceVertex(2);

            GraphPathWithDistance pathWithDistance = vertex.getPathWithDistanceFromStart();

            assertGraphPathDistance(Integer.MAX_VALUE, List.of(), pathWithDistance);
        }

        @DisplayName("it should return correct path with node distance for nonempty path")
        @Test
        void itShouldReturnCorrectPathWithDistanceForNonEmptyPath() {
            GraphDistanceVertex startVertex0 = new GraphDistanceVertex(0);
            GraphDistanceVertex pathVertex1 = new GraphDistanceVertex(1);
            pathVertex1.setDistance(2, startVertex0);

            GraphDistanceVertex pathVertex2 = new GraphDistanceVertex(2);
            pathVertex2.setDistance(5, pathVertex1);

            assertGraphPathDistance(5, List.of(0, 1), pathVertex2.getPathWithDistanceFromStart());
        }
    }

}
