package com.dpolach.example.graphs.repository;

import com.dpolach.example.graphs.Graph;
import com.dpolach.example.graphs.ModifiedAdjancencyMatrixGraph;
import com.dpolach.example.utils.matrix.Matrix;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("CacheOnlyGraphRepository tests")
@SpringBootTest
class CacheOnlyGraphRepositoryTest {

    @Autowired
    private CacheOnlyGraphRepository testedRepository;

    @BeforeEach
    void clearRepository() {
        testedRepository.remove();
    }

    @DisplayName("`find` should return empty optional when no graph is defined")
    @Test
    void itShouldReturnEmptyOptionalWhenNoGraphIsDefined() {
        assertEquals(Optional.empty(), testedRepository.find());
    }

    @DisplayName("`find` should return defined graph instance")
    @Test
    void itShouldReturnDefinedGraph() {
        Matrix definedGraphMatrix = new Matrix(new int[][]{new int[]{0, 1}, new int[]{1, 0}});
        Graph definedGraph = new ModifiedAdjancencyMatrixGraph(definedGraphMatrix.getData());

        testedRepository.save(definedGraph);
        assertEquals(Optional.of(definedGraph), testedRepository.find());
    }

}
