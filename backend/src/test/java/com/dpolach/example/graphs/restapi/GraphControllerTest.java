package com.dpolach.example.graphs.restapi;

import com.dpolach.example.graphs.Graph;
import com.dpolach.example.graphs.GraphsService;
import com.dpolach.example.graphs.ModifiedAdjancencyMatrixGraph;
import com.dpolach.example.mvc.response.ResponseMatchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("API /graph tests")
@WebMvcTest(controllers = GraphController.class)
class GraphControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GraphsService graphsServiceMock;

    @DisplayName("GET /graph tests")
    @Nested
    class GetGraphPathTests {

        private ResultActions callGetGraph() throws Exception {
            var requestBuilder = MockMvcRequestBuilders.get("/graph");
            return mockMvc.perform(requestBuilder);
        }

        @DisplayName("it should respond HTTP 200 when path was returned")
        @Test
        void itShouldRespondHttp200WhenGraphDefinitionWasReturned() throws Exception {
            Graph testData = new ModifiedAdjancencyMatrixGraph(new int[][]{{3, 0}, {0, 3}});
            when(graphsServiceMock.getDefinedGraph()).thenReturn(Optional.of(testData));

            callGetGraph().andExpect(status().isOk())
                          .andExpect(jsonPath("$.matrix[0][0]").value(3))
                          .andExpect(jsonPath("$.matrix[0][1]").value(0))
                          .andExpect(jsonPath("$.matrix[1][0]").value(0))
                          .andExpect(jsonPath("$.matrix[1][1]").value(3));
        }

        @DisplayName("it should respond HTTP 404 when graph was not defined")
        @Test
        void itShouldRespondHttp400WhenGraphIsNotDefined() throws Exception {
            callGetGraph().andExpect(status().isNotFound())
                          .andExpect(ResponseMatchers.hasResponseBodyWithErrorMessage("Graph definition doesn't exist"));
        }
    }


}
