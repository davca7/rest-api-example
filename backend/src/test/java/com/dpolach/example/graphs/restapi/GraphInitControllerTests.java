package com.dpolach.example.graphs.restapi;

import com.dpolach.example.TestResourceUtils;
import com.dpolach.example.graphs.GraphsService;
import com.dpolach.example.graphs.ModifiedAdjancencyMatrixGraph;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.dpolach.example.mvc.response.ResponseMatchers.hasResponseBodyWithErrorMessage;
import static com.dpolach.example.mvc.response.ResponseMatchers.hasResponseBodyWithErrorMessages;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("API /graph/init tests")
@WebMvcTest(controllers = GraphInitController.class)
class GraphInitControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GraphsService graphsServiceMock;

    @DisplayName("POST /graph/init tests")
    @Nested
    class PostGraphInitTests {

        ResultActions postGraphInit(String requestBody) throws Exception {
            return mockMvc.perform(MockMvcRequestBuilders.post("/graph/init")
                                                         .contentType(MediaType.APPLICATION_JSON)
                                                         .content(requestBody));
        }

        @DisplayName("it should respond with HTTP 200 when graph successfully saved")
        @Test
        void itShouldRespondHttp200WhenSuccessfullySavedGraph() throws Exception {
            int[][] expectedGraphDef = new int[][]{new int[]{0, 2}, new int[]{2, 0}};
            when(graphsServiceMock.defineGraphFromAdjancencyMatrixData(eq(expectedGraphDef))).thenReturn(new ModifiedAdjancencyMatrixGraph(expectedGraphDef));

            postGraphInit("{\"graph\":[[0, 2],[2, 0]]}").andExpect(status().isOk())
                                                        .andExpect(content().json("{\"message\":  \"Graph definition updated\"}"));
        }

        @DisplayName("it should respond with HTTP 400 when graph definition data are missing")
        @Test
        void itShouldRespondHttp404ForNullValueInGraphAttribute() throws Exception {
            postGraphInit("{\"graph\":null}").andExpect(status().isBadRequest())
                                             .andExpect(hasResponseBodyWithErrorMessage("must not be null"));
        }

        @DisplayName("it should respond with HTTP 400 for matrix with non-zero value on main diagonal")
        @Test
        void itShouldRespondHttp404ForInvalidAdjancencyMatrix() throws Exception {
            String requestBody = TestResourceUtils.readTestResourceFileContents("/com/dpolach/example/graphs/nonzeroDiagonalValueInAdjancencyMatrix.json");
            postGraphInit(requestBody).andExpect(status().isBadRequest())
                                      .andExpect(hasResponseBodyWithErrorMessages("[0, 0]: only zero values expected on main diagonal, got 1", "[1, 1]: only zero values expected on main diagonal, got 1"));

        }

        @DisplayName("it should respond with HTTP 400 for non-square matrix")
        @Test
        void itShouldRespondHttp404ForNonSquareMatrix() throws Exception {
            String requestBody = TestResourceUtils.readTestResourceFileContents("/com/dpolach/example/graphs/nonSquareMatrix.json");
            postGraphInit(requestBody).andExpect(status().isBadRequest())
                                      .andExpect(hasResponseBodyWithErrorMessage("not square matrix"));

        }

        @DisplayName("it should respond with HTTP 400 for assymetrical matrix")
        @Test
        void itShouldRespondHttp404ForAssymetricalMatrix() throws Exception {
            String requestBody = TestResourceUtils.readTestResourceFileContents("/com/dpolach/example/graphs/assymetricalSquareMatrix.json");
            postGraphInit(requestBody).andExpect(status().isBadRequest())
                                      .andExpect(hasResponseBodyWithErrorMessage("asymmetrical value at coordinates [1,0] = 1 and [0,1] = 3"));

        }

        @DisplayName("it should respond with HTTP 400 when graph data contains negative value")
        @Test
        void itShouldRespondHttp404WhenGraphDataContainsNegativeValue() throws Exception {
            String requestBody = TestResourceUtils.readTestResourceFileContents("/com/dpolach/example/graphs/matrixWithNegativeValues.json");
            postGraphInit(requestBody).andExpect(status().isBadRequest())
                                      .andExpect(hasResponseBodyWithErrorMessages("expected positive or zero values only, got -2 at coordinates [1,0]", "expected positive or zero values only, got -2 at coordinates [0,1]"));

        }

    }

}
