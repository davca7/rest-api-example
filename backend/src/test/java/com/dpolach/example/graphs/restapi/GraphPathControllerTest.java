package com.dpolach.example.graphs.restapi;

import com.dpolach.example.graphs.GraphPathWithDistance;
import com.dpolach.example.graphs.GraphsService;
import com.dpolach.example.graphs.UndefinedGraphException;
import com.dpolach.example.mvc.response.ResponseMatchers;
import org.hamcrest.collection.IsIterableContainingInOrder;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("API /graph/path tests")
@WebMvcTest(controllers = GraphPathController.class)
class GraphPathControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GraphsService graphsServiceMock;

    @DisplayName("GET /graph/path tests")
    @Nested
    class GetGraphPathTests {

        private ResultActions callGetGraphPath(Integer targetNodeParam) throws Exception {
            var requestBuilder = MockMvcRequestBuilders.get("/graph/path");
            if (targetNodeParam != null) {
                requestBuilder = requestBuilder.queryParam("targetNode", Integer.toString(targetNodeParam));
            }
            return mockMvc.perform(requestBuilder);
        }

        @DisplayName("it should respond HTTP 200 when path was returned")
        @Test
        void itShouldRespondHttp200WhenPathWasReturned() throws Exception {
            final int targetNOdeParam = 2;
            GraphPathWithDistance responseData = new GraphPathWithDistance(7, List.of(0, 3, 4));

            when(graphsServiceMock.getShortestPathToNode(targetNOdeParam)).thenReturn(Optional.of(responseData));

            callGetGraphPath(2).andExpect(status().isOk())
                               .andExpect(jsonPath("$.distance").value(7))
                               .andExpect(jsonPath("$.path").value(IsIterableContainingInOrder.contains(0, 3, 4)));
        }

        @DisplayName("it should respond HTTP 400 when targetNode is negative number")
        @Test
        void itShouldRespondHttp400WhenTargetNodeIsNegativeNumber() throws Exception {
            callGetGraphPath(-3).andExpect(status().isBadRequest())
                                .andExpect(ResponseMatchers.hasResponseBodyWithErrorMessage("targetNode: must be greater than or equal to 0"));
        }

        @DisplayName("it should respond HTTP 400 when targetNode value is missing")
        @Test
        void itShouldRespondHttp400WhenTargetNodeParamValueIsMissing() throws Exception {
            callGetGraphPath(null).andExpect(status().isBadRequest())
                                  .andExpect(ResponseMatchers.hasResponseBodyWithErrorMessage("targetNode: parameter is required but it's value is missing"));
        }

        @DisplayName("it should respond HTTP 400 when targetNode is not defined in graph")
        @Test
        void itShouldRespondHttp400WhenTargetNodeIsNotPresentInDefinedGraph() throws Exception {
            callGetGraphPath(2).andExpect(status().isBadRequest())
                               .andExpect(ResponseMatchers.hasResponseBodyWithErrorMessage("targetNode: graph node with id 2 doesn't exist in defined graph"));
        }

        @DisplayName("it should respond HTTP 400 when graph was not defined")
        @Test
        void itShouldRespondHttp400WhenGraphIsNotDefined() throws Exception {
            when(graphsServiceMock.getShortestPathToNode(anyInt())).thenThrow(new UndefinedGraphException());

            callGetGraphPath(2).andExpect(status().isBadRequest())
                               .andExpect(ResponseMatchers.hasResponseBodyWithErrorMessage("Graph definition doesn't exist"));
        }
    }


}
