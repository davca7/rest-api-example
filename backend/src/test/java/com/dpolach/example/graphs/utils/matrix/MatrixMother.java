package com.dpolach.example.graphs.utils.matrix;

import com.dpolach.example.utils.matrix.Matrix;

public class MatrixMother {
    public static int[][] createMatrixData(int colCount, int... values) {
        int[][] result = new int[values.length / colCount][colCount];

        for (int valuesIndex = 0; valuesIndex < values.length; valuesIndex++) {
            int col = valuesIndex % colCount;
            int row = valuesIndex / colCount;
            result[row][col] = values[valuesIndex];
        }

        return result;
    }

    public static Matrix createMatrix(int colCount, int... values) {
        int[][] data = createMatrixData(colCount, values);
        return new Matrix(data);
    }
}
