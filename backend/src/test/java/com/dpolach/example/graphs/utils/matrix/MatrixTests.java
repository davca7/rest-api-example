package com.dpolach.example.graphs.utils.matrix;

import com.dpolach.example.utils.matrix.Matrix;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Matrix tests")
class MatrixTests {

    @DisplayName("constructor tests")
    @Nested
    class ConstructorTests {
        @DisplayName("it should reject matrix data with different lengths of rows")
        @Test
        void itShouldRejectInvalidMatrixData() {
            int[][] invalidMatrixData = new int[][]{new int[]{1, 2}, new int[]{1, 2, 3}};
            assertThrows(IllegalArgumentException.class, () -> new Matrix(invalidMatrixData));
        }

        @DisplayName("it should reject empty matrix")
        @Test
        void itShouldRejectEmptyMatrixData() {
            int[][] invalidMatrixData = new int[][]{};
            assertThrows(IllegalArgumentException.class, () -> new Matrix(invalidMatrixData));
        }

        @DisplayName("it should create Matrix instance for valid matrix data")
        @Test
        void itShouldCreateInstanceForValidMatrixData() {
            int[][] validMatrixData = MatrixMother.createMatrixData(2, 1, 2, 2, 1);
            assertDoesNotThrow(() -> new Matrix(validMatrixData));
        }
    }

    @DisplayName("getRows tests")
    @Nested
    class GetRowsTests {
        @DisplayName("it should return correct number of rows")
        @Test
        void itShouldCorrectlyCalculateNumberOfRows() {
            Matrix testedInstance = MatrixMother.createMatrix(2, 1, 2, 2, 1, 3, 3);
            assertEquals(3, testedInstance.getRows());
        }

    }

    @DisplayName("getCols tests")
    @Nested
    class GetColsTests {
        @DisplayName("it should return correct number of cols")
        @Test
        void itShouldCorrectlyCalculateNumberOfRows() {
            Matrix testedInstance = MatrixMother.createMatrix(2, 1, 2, 2, 1, 3, 3);
            assertEquals(2, testedInstance.getCols());
        }

    }

    @DisplayName("get (matrix value) tests")
    @Nested
    class GetMatrixValueTests {

        private void assertCorrectValueOnPosition(int expectedValue, int row, int col, Matrix matrix) {
            assertEquals(expectedValue, matrix.get(col, row), "position [" + row + "," + col + "]");
        }

        @DisplayName("it should return expected values")
        @Test
        void itShouldCorrectlyCalculateNumberOfRows() {
            Matrix testedInstance = MatrixMother.createMatrix(2, 1, 2, 2, 1, 3, 4);

            assertAll(
                    () -> assertCorrectValueOnPosition(1, 0, 0, testedInstance),
                    () -> assertCorrectValueOnPosition(2, 0, 1, testedInstance),
                    () -> assertCorrectValueOnPosition(2, 1, 0, testedInstance),
                    () -> assertCorrectValueOnPosition(1, 1, 1, testedInstance),
                    () -> assertCorrectValueOnPosition(3, 2, 0, testedInstance),
                    () -> assertCorrectValueOnPosition(4, 2, 1, testedInstance)
            );
        }

        @DisplayName("it should throw ArrayIndexOutOfBoundsException for invalid column index")
        @ParameterizedTest
        @ValueSource(ints = {-1, 5})
        void itShouldThrowIllegalArgumentExceptionForInvalidColumnIndex(int invalidColumnIndex) {
            Matrix testedInstance = MatrixMother.createMatrix(2, 1, 2, 2, 1, 3, 4);

            assertThrows(ArrayIndexOutOfBoundsException.class, () -> testedInstance.get(invalidColumnIndex, 0));
        }


        @DisplayName("it should throw ArrayIndexOutOfBoundsException for invalid row index")
        @ParameterizedTest
        @ValueSource(ints = {-1, 5})
        void itShouldThrowIllegalArgumentExceptionForInvalidRowIndex(int invalidRowIndex) {
            Matrix testedInstance = MatrixMother.createMatrix(2, 1, 2, 2, 1, 3, 4);

            assertThrows(ArrayIndexOutOfBoundsException.class, () -> testedInstance.get(0, invalidRowIndex));
        }

    }

    @DisplayName("set (matrix value) tests")
    @Nested
    class SetMatrixValueTests {
        @DisplayName("it should set correct matrix item")
        @Test
        void itShouldSetValueAtCorrectCoordinates() {
            Matrix testedInstance = MatrixMother.createMatrix(3, 0, 0, 0, 0, 0, 0, 0, 0, 0);

            testedInstance.set(2, 1, -5);

            assertArrayEquals(MatrixMother.createMatrixData(3, 0, 0, 0, 0, 0, -5, 0, 0, 0), testedInstance.getData());
        }

        @DisplayName("it should throw ArrayIndexOutOfBoundsException for invalid column index")
        @ParameterizedTest
        @ValueSource(ints = {-1, 5})
        void itShouldThrowIllegalArgumentExceptionForInvalidColumnIndex(int invalidColumnIndex) {
            Matrix testedInstance = MatrixMother.createMatrix(2, 1, 2, 2, 1, 3, 4);

            assertThrows(ArrayIndexOutOfBoundsException.class, () -> testedInstance.set(invalidColumnIndex, 0, -1));
        }


        @DisplayName("it should throw ArrayIndexOutOfBoundsException for invalid row index")
        @ParameterizedTest
        @ValueSource(ints = {-1, 5})
        void itShouldThrowIllegalArgumentExceptionForInvalidRowIndex(int invalidRowIndex) {
            Matrix testedInstance = MatrixMother.createMatrix(2, 1, 2, 2, 1, 3, 4);

            assertThrows(ArrayIndexOutOfBoundsException.class, () -> testedInstance.set(0, invalidRowIndex, -1));
        }
    }

    @DisplayName("getRow tests")
    @Nested
    class GetRowTests {
        @DisplayName("it should return expected row data")
        @Test
        void itShouldReturnExpectedRowData() {
            Matrix testedInstance = MatrixMother.createMatrix(2, 1, 2, 2, 1, 3, 4);

            assertAll(
                    () -> assertArrayEquals(new int[]{1, 2}, testedInstance.getRow(0), "row index 0"),
                    () -> assertArrayEquals(new int[]{2, 1}, testedInstance.getRow(1), "row index 1"),
                    () -> assertArrayEquals(new int[]{3, 4}, testedInstance.getRow(2), "row index 2")
            );

        }

        @DisplayName("it should throw ArrayIndexOutOfBoundsException for invalid row index")
        @ParameterizedTest
        @ValueSource(ints = {-1, 4})
        void itShouldThrowArrayIndexOutOfBoundsExceptionForInvalidRowIndex(int invalidRowIndex) {
            Matrix testedInstance = MatrixMother.createMatrix(2, 1, 2, 2, 1, 3, 4);

            assertThrows(ArrayIndexOutOfBoundsException.class, () -> testedInstance.getRow(invalidRowIndex));
        }
    }

    @DisplayName("getCol tests")
    @Nested
    class GetColTests {
        @DisplayName("it should return expected column data")
        @Test
        void itShouldReturnExpectedColumnData() {
            Matrix testedInstance = MatrixMother.createMatrix(2, 1, 2, 2, 1, 3, 4);

            assertAll(
                    () -> assertArrayEquals(new int[]{1, 2, 3}, testedInstance.getCol(0), "column index 0"),
                    () -> assertArrayEquals(new int[]{2, 1, 4}, testedInstance.getCol(1), "column index 1")
            );

        }

        @DisplayName("it should throw ArrayIndexOutOfBoundsException for invalid column index")
        @ParameterizedTest
        @ValueSource(ints = {-1, 4})
        void itShouldThrowArrayIndexOutOfBoundsExceptionForInvalidColumnIndex(int invalidColumnIndex) {
            Matrix testedInstance = MatrixMother.createMatrix(2, 1, 2, 2, 1, 3, 4);

            assertThrows(ArrayIndexOutOfBoundsException.class, () -> testedInstance.getCol(invalidColumnIndex));
        }
    }

}
