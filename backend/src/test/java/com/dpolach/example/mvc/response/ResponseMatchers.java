package com.dpolach.example.mvc.response;

import org.springframework.test.web.servlet.ResultMatcher;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class ResponseMatchers {
    public static ResultMatcher hasResponseBodyWithErrorMessage(String expectedMessage) {
        return jsonPath("$.errors[*].message").value(expectedMessage);
    }

    public static ResultMatcher hasResponseBodyWithErrorMessages(String... errorMessages) {
        return jsonPath("$.errors[*].message", containsInAnyOrder(errorMessages));
    }

}
