package com.dpolach.example.validation;

import org.hamcrest.MatcherAssert;
import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class AbstractJsr303ConstraintTest {

    @Autowired
    private Validator validator;

    private String toValidationErrors(Collection<? extends ConstraintViolation<?>> violations) {
        String validationErrors = violations.stream()
                                            .map(ConstraintViolation::getMessage)
                                            .collect(Collectors.joining(", "));
        return validationErrors;
    }

    <T> void assertPassedValidation(T validatedObject) {
        Set<ConstraintViolation<T>> violations = validator.validate(validatedObject);

        assertTrue(violations.isEmpty(), "Object was rejected by validation with error(s) '" + toValidationErrors(violations) + "' while it should have been accepted");
    }

    <T> void assertValidationFailureWithError(T validatedObject, String... expectedErrorMessage) {
        Set<ConstraintViolation<T>> violations = validator.validate(validatedObject);

        List<String> actualErrors = violations.stream()
                                              .map(ConstraintViolation::getMessage)
                                              .collect(Collectors.toList());

        assertFalse(violations.isEmpty(), "Validation passed while it should be rejected");
        MatcherAssert.assertThat("unexpected error messages", actualErrors, IsIterableContainingInAnyOrder.containsInAnyOrder(expectedErrorMessage));
    }
}

