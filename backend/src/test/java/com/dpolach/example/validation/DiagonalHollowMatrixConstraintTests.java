package com.dpolach.example.validation;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class DiagonalHollowMatrixConstraintTests extends AbstractJsr303ConstraintTest {

    static class TestData {
        @DiagonalHollowMatrix
        private int[][] data;

        public TestData(int[][] data) {
            this.data = data;
        }
    }

    @DisplayName("it should accept diagonally hollow matrix")
    @Test
    void itShouldAcceptDiagonallyHollowMatrix() {

        TestData data = new TestData(new int[][]{
                new int[]{0, 2, 3},
                new int[]{2, 0, 2},
                new int[]{-5, 2, 0}
        });

        assertPassedValidation(data);
    }

    @DisplayName("it should reject matrix with non-zero value on main diagonal")
    @Test
    void itShouldRejectMatrixWithNonZeroValueOnMainDiagonal() {

        TestData data = new TestData(new int[][]{
                new int[]{0, 2, 3},
                new int[]{2, 1, 2},
                new int[]{0, 2, 0}
        });

        assertValidationFailureWithError(data, "[1, 1]: only zero values expected on main diagonal, got 1");
    }
}
