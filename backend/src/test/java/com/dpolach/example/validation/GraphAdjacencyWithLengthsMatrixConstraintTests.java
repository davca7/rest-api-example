package com.dpolach.example.validation;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class GraphAdjacencyWithLengthsMatrixConstraintTests extends AbstractJsr303ConstraintTest {

    static class TestData {
        @GraphAdjacencyWithLengthsMatrix
        private int[][] data;

        public TestData(int[][] data) {
            this.data = data;
        }

        void set(int x, int y, int value) {
            data[y][x] = value;
        }
    }

    static class DirectedGraphData {
        @GraphAdjacencyWithLengthsMatrix(directedGraph = true)
        private int[][] data;

        public DirectedGraphData(int[][] data) {
            this.data = data;
        }

        void set(int x, int y, int value) {
            data[y][x] = value;
        }
    }


    private TestData createValidUndirectedGraphData() {
        return new TestData(new int[][]{
                new int[]{0, 2, 3},
                new int[]{2, 0, 2},
                new int[]{3, 2, 0}
        });
    }

    private DirectedGraphData createValidDirectedGraphData() {
        DirectedGraphData result = new DirectedGraphData(createValidUndirectedGraphData().data);
        // create valid directed graph data but invalid undirected graph = break symmetry of edges
        result.set(1, 2, 3);
        result.set(2, 1, 0);
        return result;
    }

    @DisplayName("it should accept valid adjancency matrix (= diagonally hollow square symmetric matrix with positive values only")
    @Test
    void itShouldAcceptValidAdjancencyMatrix() {
        TestData data = createValidUndirectedGraphData();

        assertPassedValidation(data);
    }

    @DisplayName("it should reject asymmetrical matrix for undirected graph")
    @Test
    void itShouldRejectAsymmetricalMatrixForUndirectedGraph() {
        TestData data = createValidUndirectedGraphData();
        data.set(1, 2, 3);
        data.set(2, 1, 6);

        assertValidationFailureWithError(data, "asymmetrical value at coordinates [2,1] = 6 and [1,2] = 3");
    }

    @DisplayName("it should accept asymmetrical matrix for directed graph")
    @Test
    void itShouldAcceptAsymmetricalMatrixForDirectedGraph() {
        DirectedGraphData data = createValidDirectedGraphData();
        data.set(1, 2, 3);
        data.set(2, 1, 6);

        assertPassedValidation(data);
    }


    @DisplayName("it should reject matrix with non-zero value on main diagonal")
    @Test
    void itShouldRejectMatrixWithNonZeroValueOnMainDiagonal() {

        TestData data = createValidUndirectedGraphData();
        data.set(2, 2, 5);

        assertValidationFailureWithError(data, "[2, 2]: only zero values expected on main diagonal, got 5");
    }

    @DisplayName("it should reject matrix with negative value")
    @Test
    void itShouldRejectMatrixWithNegativeValue() {

        TestData data = createValidUndirectedGraphData();
        data.set(0, 2, -3);
        data.set(2, 0, -3);

        assertValidationFailureWithError(data, "expected positive or zero values only, got -3 at coordinates [0,2]", "expected positive or zero values only, got -3 at coordinates [2,0]");
    }


}
