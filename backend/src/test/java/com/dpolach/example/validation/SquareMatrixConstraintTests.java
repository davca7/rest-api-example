package com.dpolach.example.validation;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SquareMatrixConstraintTests extends AbstractJsr303ConstraintTest {

    static class TestData {
        @SquareMatrix
        private int[][] data;

        public TestData(int[][] data) {
            this.data = data;
        }
    }

    @DisplayName("it should accept square matrix")
    @Test
    void itShouldAcceptSquareMatrix() {

        TestData data = new TestData(new int[][]{
                new int[]{1, 2, 3},
                new int[]{2, 1, 1},
                new int[]{2, 1, 1}
        });

        assertPassedValidation(data);
    }

    @DisplayName("it should reject 3x2 matrix")
    @Test
    void itShouldRejectMatrixWhichIsNotSquared() {

        TestData data = new TestData(new int[][]{
                new int[]{1, 2, 3},
                new int[]{2, 1, 2}
        });

        assertValidationFailureWithError(data, "not square matrix");
    }
}
