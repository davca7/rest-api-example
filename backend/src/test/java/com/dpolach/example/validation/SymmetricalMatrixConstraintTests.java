package com.dpolach.example.validation;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SymmetricalMatrixConstraintTests extends AbstractJsr303ConstraintTest {

    static class TestData {
        @SymmetricalMatrix
        private int[][] data;

        public TestData(int[][] data) {
            this.data = data;
        }
    }

    @DisplayName("it should accept symmetrical matrix")
    @Test
    void itShouldAcceptSymmetricalMatrix() {

        TestData data = new TestData(new int[][]{
                new int[]{1, 2, 3},
                new int[]{2, 1, 2},
                new int[]{3, 2, 0}
        });

        assertPassedValidation(data);
    }

    @DisplayName("it should reject asymmetrical matrix")
    @Test
    void itShouldRejectAsymmetricalMatrix() {

        TestData data = new TestData(new int[][]{
                new int[]{1, 2, 3},
                new int[]{2, 1, 2},
                new int[]{0, 2, 0}
        });

        assertValidationFailureWithError(data, "asymmetrical value at coordinates [2,0] = 3 and [0,2] = 0");
    }
}
