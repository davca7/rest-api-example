import './App.css';
import {Container} from "react-bootstrap";
import GraphPage from "./pages/GraphPage";


function App() {

    return (
        <Container className="GraphDistance">
            <GraphPage/>
        </Container>
    );
}

export default App;
