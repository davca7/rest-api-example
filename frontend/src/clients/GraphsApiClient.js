export class FormValidationErrorResponse extends Error {
    constructor(message, errors = []) {
        super(message);
        this.validationErrors = errors;
    }
}

const handleBadRequestErrorResponse = async (response) => {
    const responseJson = await response.json();
    throw new FormValidationErrorResponse("Failed to save graph definition", responseJson.errors.map(e => e.message));
}

const handleErrorResponse = async (response) => {
    const responseText = await response.text();

    console.warn("Error response status: " + response.status);
    console.warn("Response body: " + responseText);

    throw new Error("Unexpected error response (status " + response.status + ")");
}

const GraphsApiClient = {

    /**
     * @returns {Promise<*>} promise evaulating to graph definition represented as modified distance matrix.
     */
    promiseGraphDefinition: async () => {
        const response = await fetch("/graph");
        if (response.ok) {
            const payload = await response.json();
            return payload.matrix;
        } else if (response.status === 404) {
            return [];
        } else {
            return handleErrorResponse(response);
        }
    },

    /**
     * @param targetVertexId ID of end-of-path vertex
     * @returns {Promise<void>} path from vertex with ID 0 to targetVertexId
     */
    promiseShortestPathToVertex: async (targetVertexId) => {
        const response = await fetch("/graph/path?" + new URLSearchParams({targetNode: targetVertexId}));
        if (response.ok) {
            const payloadJson = await response.json();
            payloadJson.targetNodeId = targetVertexId;
            return payloadJson;
        } else {
            return handleErrorResponse(response);
        }
    },
    promiseSubmitGraphDefinition: async (graphDefinition) => {
        const requestBody = {graph: graphDefinition};

        const response = await fetch("/graph/init", {
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify(requestBody)
        });

        if (response.status === 400) {
            return handleBadRequestErrorResponse(response);
        } else if (!response.ok) {
            return handleErrorResponse(response);
        }
    }


}

export default GraphsApiClient;

