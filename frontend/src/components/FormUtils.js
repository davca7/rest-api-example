import {Alert} from "react-bootstrap";

function translateValidationError(errorType) {
    switch (errorType) {
        case "required":
            return "Toto pole musi byt vyplneno";
        default:
            return "Nerozpoznana validacni chyba - " + errorType;
    }
}

export const FormErrorMessage = ({errors, fieldName}) => {
    const fieldError = errors?.[fieldName];
    if (!fieldError) {
        return <></>;
    }
    return <Alert variant={"danger"}>{fieldError?.message || translateValidationError(fieldError?.type)}</Alert>;
}
