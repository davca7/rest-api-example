import React from 'react';
import PropTypes from 'prop-types';

const SvgCanvas = props => (
    <svg {...props}>{props.children}</svg>
);

SvgCanvas.propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired
};

export default SvgCanvas;
