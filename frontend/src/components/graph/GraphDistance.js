import {useQuery} from "@tanstack/react-query";
import GraphsApiClient from "../../clients/GraphsApiClient";
import {useSelector} from "react-redux";

export const GraphDistance = (props) => {
    const selectedNodeId = useSelector(state => state.graph.selectedNodeId);

    const pathWithDistance = useQuery(["graph", "path", selectedNodeId], () => GraphsApiClient.promiseShortestPathToVertex(selectedNodeId), {enabled: selectedNodeId !== null});

    if (pathWithDistance.isInitialLoading) {
        return <h2>Loading....</h2>;
    } else if (pathWithDistance.isError) {
        return <h2>Failed to load ... </h2>;
    } else if (pathWithDistance.data == null) {
        return <h2>Klikněte na uzel k zobrazení nejkratší cesty a vzdálenosti z uzlu 0</h2>;
    }

    const {distance, targetNodeId, path} = pathWithDistance.data;
    return <h2>Vzdálenost je {distance}</h2>
}

GraphDistance.propTypes = {}

