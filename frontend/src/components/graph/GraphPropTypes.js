import PropTypes from "prop-types";

export const GraphPropTypes = {
    vertexId: () => PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    distanceMatrix: () => PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number)),
    graphPath: () => PropTypes.arrayOf(GraphPropTypes.vertexId()),
    pathWithDistance: () => PropTypes.shape({
        path: GraphPropTypes.graphPath(),
        distance: PropTypes.number,
        targetNodeId: GraphPropTypes.vertexId()
    }),
}
