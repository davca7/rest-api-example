import {createSlice} from '@reduxjs/toolkit';

export const GraphSlice = createSlice({
    name: "graph",
    initialState: {
        selectedNodeId: null
    },
    reducers: {
        selectNode: (state, {payload}) => {
            state.selectedNodeId = payload;
        },
        clearSelectedNode: (state) => {
            state.selectedNodeId = null;
        }
    }
});

export const {selectNode, clearSelectedNode} = GraphSlice.actions;

export default GraphSlice.reducer;
