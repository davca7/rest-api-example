import {useCallback, useEffect, useMemo, useState} from "react";
import {useMutation, useQuery, useQueryClient} from "@tanstack/react-query";
import {Alert, Button, Form, FormControl} from "react-bootstrap";
import PropTypes from "prop-types";
import GraphsApiClient from "../../../clients/GraphsApiClient";
import {useDispatch} from "react-redux";
import {clearSelectedNode} from "../GraphSlice";
import {useForm} from "react-hook-form";
import {GraphPropTypes} from "../GraphPropTypes";
import {FormErrorMessage} from "../../FormUtils";

function distanceMatrixToText(distanceMatrixData) {
    return distanceMatrixData.map(row => row.join(" ")).join("\n");
}

function textToDistanceMatrix(distanceMatrixAsText) {
    return distanceMatrixAsText.split("\n").map(line => line.split(" ").map(t => 1 * t));
}

export const GraphDistanceMatrixForm = ({formData, onSubmit}) => {
    const {
        register,
        handleSubmit,
        reset,
        formState: {errors, isSubmitting, isDirty}
    } = useForm();

    const [submitErrors, setSubmitErrors] = useState([]);

    useEffect(() => {
        const convertedFormData = {
            ...formData,
            matrix: distanceMatrixToText(formData.matrix)
        };
        reset(convertedFormData);
    }, [formData]);

    const submitForm = async (formData) => {
        try {
            await onSubmit({
                ...formData,
                matrix: textToDistanceMatrix(formData.matrix)
            });
        } catch (error) {
            if (error.validationErrors) {
                setSubmitErrors(error.validationErrors);
            } else {
                setSubmitErrors(["Neočekávaný problém"]);
            }
            setTimeout(() => setSubmitErrors([]), 5000);
        }
    };

    const borderColorClass = isDirty ? "border-warning border-3" : "border-success border-2";

    return <Form onSubmit={handleSubmit(submitForm)}>
        <FormControl as={"textarea"} {...register("matrix", {required: true})} rows={11} cols={30}
                     className={"square border " + borderColorClass} aria-invalid={!!errors.matrix}/>
        <FormErrorMessage errors={errors} fieldName={"matrix"}/>
        <Button type={"submit"} disabled={isSubmitting || !isDirty}>Uložit graf</Button>
        <Button disabled={isSubmitting || !isDirty} onClick={() => reset()}>Zahodit změny</Button>
        {submitErrors?.length > 0 ? <Alert variant={"danger"}>Uložení selhalo: {submitErrors.join(", ")}</Alert> : null}
    </Form>;
}

GraphDistanceMatrixForm.propTypes = {
    onSubmit: PropTypes.func,
    formData: PropTypes.shape({
        matrix: GraphPropTypes.distanceMatrix()
    })
};

const ApiContainer = () => {
    const dispatch = useDispatch();
    const queryClient = useQueryClient();
    const {isLoading, isError, data} = useQuery(["graph", "definition"], GraphsApiClient.promiseGraphDefinition);

    const {mutateAsync} = useMutation((matrixData) => GraphsApiClient.promiseSubmitGraphDefinition(matrixData), {
        onSuccess: async () => {
            dispatch(clearSelectedNode());
            return queryClient.invalidateQueries(["graph"]);
        }
    });

    const formData = useMemo(() => {
        return {
            matrix: data
        }
    }, [data]);

    const formSubmitCallback = useCallback(async (formData) => {
        return mutateAsync(formData.matrix);
    }, []);

    if (isLoading) {
        return <div>Loading data...</div>;
    } else if (isError) {
        return <div>Failed to load data</div>;
    } else {
        return <GraphDistanceMatrixForm formData={formData} onSubmit={formSubmitCallback}/>;
    }

}

GraphDistanceMatrixForm.ApiContainer = ApiContainer;
