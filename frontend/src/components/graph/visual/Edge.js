import React from 'react';
import PropTypes from 'prop-types';
import {GraphPropTypes} from "../GraphPropTypes";
import style from "./Graph.module.css";

const Edge = props => {

    const textX = (props.from.x + props.to.x) / 2;
    const textY = (props.from.y + props.to.y) / 2;

    const classNames = props.selected ? `${style.edge} ${style.selected}` : style.edge;

    return (
        <g className={classNames}>
            <line x1={props.from.x} y1={props.from.y} x2={props.to.x} y2={props.to.y}/>
            <text x={textX} y={textY}>{props.length}</text>
        </g>
    );
};

const vertexDefShape = PropTypes.shape({
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    id: GraphPropTypes.vertexId().isRequired
});

Edge.propTypes = {
    from: vertexDefShape.isRequired,
    to: vertexDefShape.isRequired,
    length: PropTypes.number.isRequired,
    color: PropTypes.string
};

export default Edge;
