import React, {useMemo} from 'react';
import Vertex from "./Vertex";
import Edge from "./Edge";
import SvgCanvas from "../../SvgCanvas";
import PropTypes from "prop-types";
import {GraphPropTypes} from "../GraphPropTypes";
import styles from "./Graph.module.css";

const createVertexUiFactory = (highlightPath = [], onVertexClickCallback = () => {
}) => {
    return (vertexData) => {
        const props = {...vertexData};
        props.selected = highlightPath.indexOf(vertexData.id) > -1;
        return <Vertex key={vertexData.id} {...props} onClick={onVertexClickCallback}/>;
    }
}

const createEdgeUiFactory = (vertices, path) => {
    return (edge) => {
        const vertexStart = vertices.find(v => v.id === edge.fromId);
        if (!vertexStart) {
            throw new Error("Cannot find vertex with id " + edge.fromId + " defined as start point for edge. Known vertices are " + vertices.map(v => v.id));
        }
        const vertexEnd = vertices.find(v => v.id === edge.toId);
        if (!vertexEnd) {
            throw new Error("Cannot find vertex with id " + edge.toId + " defined as end point for edge. Known vertices are " + vertices.map(v => v.id));
        }

        const isOnPath = path.indexOf(edge.fromId) > -1 && path.indexOf(edge.toId) > -1 && ((path.indexOf(edge.fromId) + 1 === path.indexOf(edge.toId)) || path.indexOf(edge.fromId) - 1 === path.indexOf(edge.toId));

        const props = {from: vertexStart, to: vertexEnd, length: edge.length};
        props.selected = isOnPath;

        return <Edge key={edge.fromId + "-" + edge.toId} {...props}/>;
    }
};

function createGraphVerticesData(verticeIds, sizeX, sizeY, marginX = 50, marginY = 50) {
    const verticesCount = verticeIds.length;
    const angleDelta = Math.PI * 2 / verticesCount;
    let angle = 0;

    const offsetX = sizeX / 2;
    const offsetY = sizeY / 2;

    const result = [];
    for (const vertexId of verticeIds) {
        const vertex = {id: vertexId};
        vertex.x = ((offsetX - marginX) * Math.sin(angle)) + offsetX;
        vertex.y = ((offsetY - marginY) * Math.cos(angle) * -1) + offsetY;
        result.push(vertex);
        angle += angleDelta;
    }
    return result;
}

function convertModifiedDistanceMatrixToGraphData(modifiedDistanceMatrix = []) {
    const result = {
        vertices: [],
        edges: []
    };

    for (let rowId = 0; rowId < modifiedDistanceMatrix.length; rowId++) {
        result.vertices.push(rowId);

        for (let colId = 0; colId < rowId; colId++) {
            const edgeLength = modifiedDistanceMatrix[rowId][colId];
            if (edgeLength > 0) {
                result.edges.push({fromId: rowId, toId: colId, length: edgeLength})
            }
        }
    }

    return result;
}

const Graph = ({width, height, graph, onVertexClick, highlightPath = [], ...otherProps}) => {

    const vertices = (graph && graph.vertices) || [];
    const edges = (graph && graph.edges) || [];

    const verticesUiData = useMemo(() => createGraphVerticesData((vertices) || [], width, height), [vertices, height, width]);

    return (
        <SvgCanvas width={width} height={height} className={styles.graph}>
            <rect width={"100%"} height={"100%"}/>

            {edges.map(createEdgeUiFactory(verticesUiData, highlightPath))}

            {verticesUiData.map(createVertexUiFactory(highlightPath, onVertexClick))}
        </SvgCanvas>
    );
};

Graph.createPropsFromDistanceMatrixAndPath = (distanceMatrix = [], path = []) => {
    return {
        path: path,
        graph: convertModifiedDistanceMatrixToGraphData(distanceMatrix)
    };
}

const EdgeDefinition = {
    fromId: GraphPropTypes.vertexId().isRequired,
    toId: GraphPropTypes.vertexId().isRequired,
    length: PropTypes.number
};

const GraphDefinition = {
    vertices: PropTypes.arrayOf(GraphPropTypes.vertexId()).isRequired,
    edges: PropTypes.arrayOf(PropTypes.shape(EdgeDefinition))
}

Graph.propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    graph: PropTypes.shape(GraphDefinition),
    highlightPath: GraphPropTypes.graphPath(),
    onVertexClick: PropTypes.func
};

const DistanceMatrixGraph = (props) => {
    const graphDef = convertModifiedDistanceMatrixToGraphData(props.distanceMatrix);

    const updatedProps = {
        ...props,
        distanceMatrix: null,
        graph: graphDef
    };

    return <Graph {...updatedProps} />
};

Graph.DistanceMatrixGraph = DistanceMatrixGraph;

DistanceMatrixGraph.propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    distanceMatrix: GraphPropTypes.distanceMatrix().isRequired,
    highlightPath: PropTypes.arrayOf(GraphPropTypes.vertexId())
}

export default Graph;


