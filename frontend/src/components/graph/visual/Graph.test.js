import {render, screen} from '@testing-library/react';
import Graph from "./Graph";

const graphDef = {
    vertices: ["A", "B", "C"],
    edges: [{fromId: "A", toId: "B", length: 2}, {fromId: "C", toId: "A", length: 4}]
};

test('renders defined vertices', () => {
    render(<Graph width={100} height={100} graph={graphDef}/>);

    const verticeLabelHtml = graphDef.vertices.map(verticeId => screen.getByText(verticeId));

    expect(verticeLabelHtml).toHaveLength(3);
    verticeLabelHtml.forEach(verticesHtml => expect(verticesHtml).toBeInTheDocument());
});

test('renders defined edges', () => {
    render(<Graph width={100} height={100} graph={graphDef}/>);

    const edgesHtml = graphDef.edges.map(e => e.length).map(verticeId => screen.getByText(verticeId));

    expect(edgesHtml).toHaveLength(2);
    edgesHtml.forEach(edgeLengthHtml => expect(edgeLengthHtml).toBeInTheDocument());
});

test("highlights vertices on highlightPath", () => {
    render(<Graph width={100} height={100} graph={graphDef} highlightPath={["C", "A"]}/>)

    const pathVertices = screen.getAllByText(/[CA]/);
    expect(pathVertices).toHaveLength(2);
    pathVertices.forEach(p => expect(p.parentNode).toHaveClass("selected"));
})
