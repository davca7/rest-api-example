import React from 'react';
import PropTypes from 'prop-types';
import {GraphPropTypes} from "../GraphPropTypes";
import style from "./Graph.module.css";

const Vertex = ({id, x, y, size = 25, selected, onClick}) => {
    const classNames = selected ? `${style.vertex} ${style.selected}` : style.vertex;

    return (
        <g className={classNames} transform={`translate(${x}, ${y})`}>
            <circle cx={0} cy={0} r={size} onClick={() => onClick(id)}/>
            <text x={0} y={0} onClick={() => onClick(id)}>{id}</text>
        </g>
    );
};

Vertex.propTypes = {
    id: GraphPropTypes.vertexId().isRequired,
    x: PropTypes.number.isRequired,
    y: PropTypes.number.isRequired,
    selected: PropTypes.bool,
    onClick: PropTypes.func,
    size: PropTypes.number
};

export default Vertex;
