import {useDispatch, useSelector} from "react-redux";
import {useQuery} from "@tanstack/react-query";
import GraphsApiClient from "../clients/GraphsApiClient";
import {selectNode} from "../components/graph/GraphSlice";
import PropTypes from "prop-types";
import React, {useCallback} from "react";
import Graph from "../components/graph/visual/Graph";

const DeclaredGraph = (props) => {
    const dispatch = useDispatch();
    const selectedNodeId = useSelector(state => state.graph.selectedNodeId);
    const distanceMatrix = useQuery(["graph", "definition"], GraphsApiClient.promiseGraphDefinition);
    const pathWithDistance = useQuery(["graph", "path", selectedNodeId], () => GraphsApiClient.promiseShortestPathToVertex(selectedNodeId), {enabled: selectedNodeId !== null});
    const onVertexClickCallback = useCallback((nodeId) => dispatch(selectNode(nodeId)), [selectedNodeId]);

    if (distanceMatrix.isLoading) {
        return <div>Loading data...</div>;
    }

    if (distanceMatrix.isError || pathWithDistance.isError) {
        return <div>Failed to load data...</div>;
    }

    const fullPathToTargetNode = pathWithDistance.isLoading ? [] : [...pathWithDistance.data.path, pathWithDistance.data.targetNodeId];

    return <Graph.DistanceMatrixGraph {...props} distanceMatrix={distanceMatrix.data}
                                      highlightPath={fullPathToTargetNode}
                                      onVertexClick={onVertexClickCallback}/>
};

DeclaredGraph.propTypes = {
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired
}

export default DeclaredGraph;
