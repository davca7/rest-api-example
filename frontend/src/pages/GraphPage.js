import React, {useMemo} from 'react';
import {Button, Col, Row} from "react-bootstrap";
import {GraphDistance} from "../components/graph/GraphDistance";
import {GraphDistanceMatrixForm} from "../components/graph/edit/GraphDistanceMatrixForm";
import {useMutation, useQueryClient} from "@tanstack/react-query";
import GraphsApiClient from "../clients/GraphsApiClient";
import DeclaredGraph from "../features/DeclaredGraph";
import {useDispatch} from "react-redux";
import {clearSelectedNode} from "../components/graph/GraphSlice";
import {ErrorBoundary} from "react-error-boundary";


const graphExample1 = [
    [0, 4, 2],
    [4, 0, 1],
    [2, 1, 0]
];
const graphExample2 = [
    [0, 6, 0, 1, 0],
    [6, 0, 5, 2, 2],
    [0, 5, 0, 0, 5],
    [1, 2, 0, 0, 1],
    [0, 2, 5, 1, 0]
];

function ExampleButtons() {
    const queryClient = useQueryClient();
    const dispatch = useDispatch();

    const submitGraphExampleDef = useMutation(matrixData => GraphsApiClient.promiseSubmitGraphDefinition(matrixData), {
        onSettled: () => {
            setTimeout(() => submitGraphExampleDef.reset(), 5000);
        },
        onSuccess: () => {
            queryClient.invalidateQueries(["graph"]);
        }
    });

    const buttonClassNames = useMemo(() => {
        if (submitGraphExampleDef.isLoading) {
            return "btn-secondary disabled";
        } else {
            return "btn-secondary";
        }
    }, [submitGraphExampleDef.isLoading]);

    const resetSelectedNode = () => dispatch(clearSelectedNode());

    return (<><Button className={buttonClassNames}
                      onClick={() => submitGraphExampleDef.mutate(graphExample1, {onSuccess: () => resetSelectedNode()})}>Ukázka
        1</Button>
        <Button className={"btn-secondary"}
                onClick={() => submitGraphExampleDef.mutate(graphExample2, {onSuccess: () => resetSelectedNode()})}>Ukázka
            2</Button></>);

}


function GraphPage() {

    return (
        <>
            <Row><h1>Dijkstra: nejkratší cesta a vzdálenost v neorientovaném grafu</h1></Row>

            <ErrorBoundary fallbackRender={({error, resetErrorBoundary, componentStack}) => {
                return <Row>
                    <Col>
                        <h2>An error occured: {error.message}</h2>
                        <Button className={"btn-warning"} onClick={resetErrorBoundary}>Try again</Button>
                    </Col>
                </Row>;
            }}>
                <Row>
                    <Col>
                        <DeclaredGraph width={500} height={500}/><br/>
                        <GraphDistance/>
                    </Col>
                    <Col>
                        <Row>
                            <Col>
                                Graf je zadán pomocí modifikované matice souslednosti která má následující pravidla:
                                <ul>
                                    <li>
                                        Hodnota 0 znamená že vrcholy mezi sebou nemají přímé spojení
                                    </li>
                                    <li>
                                        Hodnota větší než 0 znamená že vrcholy mezi sebou mají přímé spojení a tato
                                        hodnota
                                        definuje vzdálenost mezi takovými vrcholy
                                    </li>
                                    <li>
                                        Matice musí být symetrická (graf musí být neorientovaný)
                                    </li>
                                    <li>
                                        Hodnoty na hlavní diagonále musí být 0
                                    </li>
                                    <li>
                                        Žádná hodnota nesmí být záporná
                                    </li>
                                </ul>
                                <ExampleButtons/>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <GraphDistanceMatrixForm.ApiContainer/>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </ErrorBoundary>
        </>
    )
        ;

}

GraphPage.propTypes = {}

export default GraphPage;
