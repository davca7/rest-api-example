import {combineReducers, configureStore} from '@reduxjs/toolkit';

import graph from './components/graph/GraphSlice';

const rootReducer = combineReducers({
    graph
})

export default configureStore({
    reducer: rootReducer
});
